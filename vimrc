" gibwar's vim config file

set nocompatible

" set encoding to utf-8 and don't allow vim to attempt to convert any
" encodings.
set encoding=utf-8
set fileencodings=

" default to use unix line endings for new files.
set fileformats=unix,dos
set fileformat=unix

" set up directory global variables based on OS setup
let s:user_vim_dir = split(&runtimepath, ',')
if len(s:user_vim_dir) > 0
    " normalize path to unix-style, windows is fine with this
    let s:user_vim_dir = fnamemodify(s:user_vim_dir[0], ':p:h:gs?\\?/?')
    let g:xdg_cache = s:user_vim_dir
    let g:xdg_config = s:user_vim_dir
    if has('unix')
        if !empty($XDG_CACHE_HOME)
            let g:xdg_cache = $XDG_CACHE_HOME . '/vim'
        endif

        if !empty($XDG_CONFIG_HOME)
            let g:xdg_config = $XDG_CONFIG_HOME . '/vim'
        endif
    endif
elseif has('unix')
    let g:xdg_cache = $HOME . '/.vim'
    let g:xdg_config = $HOME .'/.vim'
elseif has('win32')
    let g:xdg_cache = $HOME . '/vimfiles'
    let g:xdg_config = $HOME . '/vimfiles'
endif

" enable syntax and filetypes
syntax on
filetype plugin indent on
