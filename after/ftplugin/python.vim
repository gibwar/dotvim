call undoftplugin#setl('expandtab')
call undoftplugin#setl('shiftwidth', 4)
call undoftplugin#setl('softtabstop', 4)
call undoftplugin#setl('tabstop', 8)
call undoftplugin#setl('textwidth', 100)
call undoftplugin#setl('foldmethod', 'indent')
call undoftplugin#setl('foldlevel', 2)

nnoremap <buffer> <silent> <F8> :<C-u>call compile#run('flake8', 'pylint')<CR>
