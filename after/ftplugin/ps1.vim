runtime shared/common-settings.vim
runtime shared/ensure-undo-ftplugin.vim

setlocal expandtab
setlocal shiftwidth=4
setlocal softtabstop=4

let b:undo_ftplugin .= '|setlocal expandtab<'
let b:undo_ftplugin .= '|setlocal shiftwidth<'
let b:undo_ftplugin .= '|setlocal softtabstop<'
