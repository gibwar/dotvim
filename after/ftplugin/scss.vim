runtime shared/common-settings.vim

" scss files use 2 spaces for indentation.
setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2
