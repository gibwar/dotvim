" vim filetype settings

setlocal autoindent
setlocal expandtab
setlocal foldmethod=marker
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal tabstop=8
setlocal textwidth=100

setlocal formatoptions-=o
setlocal formatoptions-=l
