" Autoload session files when launched, if available.
" Last Change: 2020-04-20
" Maintainer: Joshua <gibwar@gibixonline.com>
" License: Free to do what ever you want, it's in the public domain.

if exists('g:loaded_session_autoload')
    finish
endif
let g:loaded_session_autoload = 1

let s:save_cpo = &cpo
set cpo&vim

let g:session_autoload_save_at_exit = 0
let s:original_cwd = getcwd()

" Removes preceding backslash from % and # characters (ex: '\#\%' to '#%')
let s:UnPercent = {list -> map(copy(list), {_, val -> substitute(val, '\v\\(\%|#)', '\1', 'g')})}
" Add \ in front of % or # if it doesn't already have one
let s:ScrubPath = {path -> substitute(path, '\v\\@<!(\%|#)', '\\\1', 'g')}

function s:Message(...) abort
    echohl None
    echom 'session-autoload: ' . join(s:UnPercent(a:000), ' ')
endfunction

function s:Warning(...) abort
    echohl WarningMsg
    echom 'session-autoload: ' . join(s:UnPercent(a:000), ' ')
    echohl None
endfunction

function s:MakeSession(session) abort
    execute 'mksess! ' . s:ScrubPath(a:session)
    call s:Message('session saved at:', a:session)
    let g:session_autoload_save_at_exit = 1
endfunction

" Normalizes the provided path to use forward slashes. Windows is perfectly fine with this.
function! s:NormalizePath(dir) abort
    return fnamemodify(a:dir, ':p:h:gs?\\?/?')
endfunction

function! s:GetUserSessionDirectory() abort
    let l:user_vim_dir = split(&runtimepath, ',')
    if len(l:user_vim_dir) == 0
        return ''
    endif

    let l:user_vim_dir = s:NormalizePath(l:user_vim_dir[0]) . '/sessions'
    return l:user_vim_dir
endfunction

function! s:ConvertDirectoryToFile() abort
    let l:session_file = substitute(s:original_cwd, '[:\\/]', '%', 'g') . '.vim'
    return l:session_file
endfunction

function s:FindGitDir() abort
    let l:git_dir = finddir('.git', '.,;;')
    if l:git_dir is# ''
        return ''
    endif

    return s:NormalizePath(l:git_dir)
endfunction

function s:FindSessionFile(dir) abort
    if match(a:git, '\.git$') >= 0
        let session_file = a:dir . '/Session.vim'
        if filereadable(session_file)
            return session_file
        endif
    endif
endfunction

function s:GetGitSessionFile() abort
    let git_dir = s:FindGitDir()
    if git_dir ==# ''
        return
    endif

    let session_file = git_dir . '/Session.vim'
    if filereadable(session_file)
        return session_file
    endif

    return ''
endfunction

function s:GetSessionDirSessionFile() abort
    let session_dir= s:GetUserSessionDirectory()
    let session_file = session_dir . '/' . s:ConvertDirectoryToFile()
    return substitute(session_file, '\v\\(\%|#)', '\1', 'g')
endfunction

function s:FindAndLoadSession() abort
    if exists('g:disable_session_autoload')
        return
    endif

    " if files were loaded from the command line, abort loading any session
    " files to prevent disorienting the user. Address #1.
    if argc() != 0
        return
    endif

    let session_file = s:GetGitSessionFile()
    if session_file ==# ''
        let session_file = s:GetSessionDirSessionFile()
    endif

    if !filereadable(session_file)
        call s:Message('no session found to autoload. Tried ' . session_file)
        return
    endif

    function s:LoadSession(tid) closure abort
        call s:Message('loading session ' . session_file)
        let l:session_file = s:ScrubPath(l:session_file)
        exec 'source ' . session_file

        if exists('v:this_session')
            let g:session_autoload_save_at_exit = 1
        endif
    endfunction

    call timer_start(0, funcref('s:LoadSession'))
endfunction

function s:AutoSaveSessionAtExit() abort
    if g:session_autoload_save_at_exit != 1 || !exists('v:this_session')
        return
    endif

    call s:MakeSession(v:this_session)
endfunction

function s:SaveSession(session_name) abort
    if exists('v:this_session') && v:this_session isnot# ''
        call s:MakeSession(v:this_session)
        return
    endif

    let l:git_dir = s:FindGitDir()
    let l:result = l:git_dir isnot# ''
    if l:git_dir isnot# ''
        let l:session_name = a:session_name
        if l:session_name is# ''
            let l:session_name = 'Session.vim'
        endif

        let l:session = l:git_dir . '/' . l:session_name
        call s:MakeSession(l:session)
        return
    endif

    let l:user_session_dir = s:GetUserSessionDirectory()
    if l:user_session_dir is# ''
        call s:Warning('unable to determine session directory')
        return
    endif

    if filewritable(l:user_session_dir) != 2
        let l:message = 'session-autoload: session directory "'
            \ . l:user_session_dir . '" does not exist. Create it?'

        let l:create_dir = confirm(l:message, "&Yes\n&No", 2, 'Question')
        if l:create_dir != 1
            call s:Warning('please create session directory "' . l:user_session_dir . '"')
            return
        endif

        let v:errmsg = ''
        silent! call mkdir(l:user_session_dir, 'p', 0700)
        if v:errmsg isnot# ''
            echohl ErrorMsg
            echomsg 'session-autoload: could not create directory '
                \ . l:user_session_dir . '. Please create session directory manually.'
            echomsg v:errmsg
            echohl None
            return
        endif
    endif

    let l:session = l:user_session_dir . '/' . s:ConvertDirectoryToFile()
    call s:MakeSession(l:session)
endfunction

function s:ToggleSave() abort
    if !exists('v:this_session')
        echohl WarningMsg
        echomsg 'session-autoload: no session is loaded.'
        echohl None
        return
    endif

    if g:session_autoload_save_at_exit == 0
        let g:session_autoload_save_at_exit = 1
        echomsg 'session-autoload: session will be updated at exit.'
    else
        let g:session_autoload_save_at_exit = 0
        echomsg 'session-autoload: session will not be updated at exit.'
    endif
endfunction

augroup session_autoload_cmds
    autocmd!
    autocmd VimEnter * :call <SID>FindAndLoadSession()
    autocmd VimLeavePre * :call <SID>AutoSaveSessionAtExit()
augroup END

if !hasmapto('<Plug>SessionAutoloadToggleSave')
    map <unique> <Leader><F4> <Plug>SessionAutoloadToggleSave
endif

nnoremap <unique> <script> <Plug>SessionAutoloadToggleSave :call <SID>ToggleSave()<CR>
command! -nargs=? -complete=file SessionAutoloadSaveSession :call <SID>SaveSession(<q-args>)

let &cpo = s:save_cpo
unlet s:save_cpo
