" Various file management settings

" enable cross-platform temporary directories
function s:EnsureCacheOption(opt, path) abort
    let l:target = g:xdg_cache . '/' . a:path
    if !isdirectory(l:target)
        call mkdir(l:target, 'p', '0700')
    endif

    exec 'set ' . a:opt . '=' . l:target . '//'
endfunction

call s:EnsureCacheOption('backupdir', 'backup')
call s:EnsureCacheOption('directory', 'swap')
call s:EnsureCacheOption('undodir', 'undo')

delfunction s:EnsureCacheOption

set autoread
set backup
set undofile
let &viminfofile=g:xdg_cache . '/viminfo'
set writebackup

" Enable modeline reading
set modeline
set modelines=5

if has('mksession')
    set sessionoptions-=blank
    set sessionoptions-=folds
    set sessionoptions-=help
    set sessionoptions-=options
    set sessionoptions-=terminal
    set viewoptions-=folds
    set viewoptions-=options

    "let g:disable_session_autoload = 1
endif
