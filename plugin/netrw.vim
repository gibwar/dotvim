" don't occupy the alt file
let g:netrw_altfile=1

" split above
let g:netrw_alto=0

" split left
let g:netrw_altv=0

" disable banner
let g:netrw_banner=0

" set the buffer to enable numbers as well as the current defaults
let g:netrw_bufsettings='noma nomod nobl nowrap ro nu nornu nohls'

" move the history file to a better location
let g:netrw_home=g:xdg_cache

" windows 10 scp uses windows path
let g:netrw_cygwin=0

" silent transfers
let g:netrw_silent=1

" long listing by default
let g:netrw_liststyle=1
