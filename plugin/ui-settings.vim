" Handles various UI settings for the terminal and GUI.

" Enable termguicolors
if has('termguicolors')
    " check if the term is high color but the foreground and
    " background codes didn't get set
    if &term =~ '256color' && empty(&t_8b) && empty(&t_8f)
        let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
        let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
    endif

    set termguicolors
endif

silent! packadd edge

if !exists('g:lightline')
    let g:lightline = {}
endif

if has#colorscheme('edge')
    set background=dark
    let g:edge_style = 'neon'
    let g:edge_better_performance = 1
    let g:edge_diagnostic_text_highlight = 1
    let g:edge_diagnostic_line_highlight = 1
    let g:edge_diagnostic_virtual_text = 'colored'
    let g:edge_dim_foreground = 0
    let g:edge_disable_italic_comment = 1
    let g:edge_enable_italic = 0
    let g:edge_show_eob = 1
    let g:edge_transparent_background = 0

    let g:lightline.colorscheme = 'edge'
    colorscheme edge
elseif has#colorscheme('everforest')
    set background=dark
    let g:everforest_background = 'hard'
    let g:everforest_enable_italic = 0
    let g:everforest_disable_italic_comment = 1

    let g:lightline.colorscheme = 'everforest'
    colorscheme everforest
elseif has#colorscheme('apprentice')
    set background=dark

    let g:lightline.colorscheme = 'apprentice'
    colorscheme apprentice
endif

" GUI Settings {{{
if has('gui_running')
    if has('win32')
        " Require use of the DejaVu Sans Mono Nerd Font
        set guifont=DejaVuSansMono_NF:h11,DejaVuSansMono_NFM:h11
    elseif has('linux')
        let size = 16
        if str2float($GDK_DPI_SCALE) == 1.5
            let size = 11
        endif

        let &guifont="DejaVu Sans Mono Nerd Font " .. size
    endif

    " Disable various GUI options, I prefer it to look like a terminal
    " I do like having (c)onsole dialogs.
    " I don't like the (Ll)eft or (Rr)ight scroll bars, the (T)oolbar,
    " (e)gui tabs, or (m)enus.
    set guioptions+=c
    set guioptions-=L
    set guioptions-=R
    set guioptions-=T
    set guioptions-=e
    set guioptions-=l
    set guioptions-=m
    set guioptions-=r

    " 'Maximize' the window
    set linespace=2
    set columns=120
    set lines=9999

    if has('linux')
        set guiheadroom=0
        set lines=65

        call timer_start(400, {-> execute('if &lines < 65 | set lines=65 | endif') })
    endif

    " Set DirectX settings for better rendering on Windows.
    "if has('directx')
    "    set renderoptions=type:directx
    "    set renderoptions+=gamma:1.8
    "    set renderoptions+=contrast:1
    "    set renderoptions+=level:0.5
    "    set renderoptions+=geom:1
    "    set renderoptions+=renmode:5
    "    set renderoptions+=taamode:1
    "endif
endif
" }}}
