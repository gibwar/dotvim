silent! packadd lightline

set laststatus=2
set noshowmode

let s:lightline = {}
if exists('g:lightline')
    let s:lightline = g:lightline->copy()
endif

let g:lightline = {
    \ 'active': {
        \ 'left': [
            \ ['mode', 'paste', 'spell'],
            \ ['gitbranch', 'truncate', 'filename'],
        \ ],
        \ 'right': [
            \ ['lineinfo'],
            \ ['percent'],
            \ ['cocstatus', 'fileformat', 'fileencoding', 'filetype'],
        \ ],
    \ },
    \ 'component': {
        \ 'cocstatus': '%{%LightlineCocStatus()%}',
        \ 'filename': '%{%LightlineFilename()%}',
        \ 'truncate': '%<',
    \ },
    \ 'component_function': {
        \ 'gitbranch': 'LightlineGitBranch',
    \ },
    \ 'component_type': {
        \ 'cocstatus': 'raw',
        \ 'filename': 'raw',
        \ 'truncate': 'raw',
    \ },
    \ 'component_visible_condition': {
        \ 'cocstatus':
            \ "exists('b:coc_diagnostic_info') && " ..
            \ "max(values(filter(copy(b:coc_diagnostic_info), 'type(v:val) == v:t_number')))",
        \ 'truncate': 0,
    \ },
\ }

function! LightlineCocStatus() abort
    let info = get(b:, 'coc_diagnostic_info', {})
    if empty(info)
        return ''
    endif

    let messages = []

    if get(info, 'error', 0)
        call add(messages, '%#CocErrorFloat#' .. info.error .. "\uf00d")
    endif

    if get(info, 'warning', 0)
        call add(messages, '%#CocWarningFloat#' .. info.warning .. "\uf12a")
    endif

    if get(info, 'information', 0)
        call add(messages, '%#CocInfoFloat#' .. info.information .. "\uf128")
    endif

    if get(info, 'hint', 0)
        call add(messages, '%#CocHintFloat#' .. info.hint .. "\uf067")
    endif

    if empty(messages)
        return ''
    endif

    let active = ''

    return join(messages, ' ') .. '%#LightlineRight_active_2# '
endfunction

function! LightlineFilename() abort
    let filename = expand('%:f')
    let name_length = 0
    if filename ==# ''
        let name_length = 9
        let filename = '[No Name]'
    else
        " sometimes %:f comes in as the full path even though it's under the cwd
        let filename = substitute(fnamemodify(filename, ':.'), '%', '%%', 'g')
        let name_length = strchars(fnamemodify(filename, ':t'))
    endif

    let readonly = &readonly ? "\uf023 " : ''
    let modified = &modified ? " \u2022" : ''
    let name_length = strchars(readonly) + name_length + strchars(modified) + 2
    return '%-' .. [name_length, 50]->min() . '.50( ' .. readonly . filename . modified .. ' %)'
endfunction

let s:loaded_gitbranch = 0

function! LightlineGitBranch() abort
    if winwidth(0) > 70
        if s:loaded_gitbranch == 0
            packadd vim-gitbranch
            let s:loaded_gitbranch = 1
        endif

        return gitbranch#name()
    endif

    return ''
endfunction

call extend(g:lightline, s:lightline)
