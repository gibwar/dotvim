" If available, set RipGrep as our grep program.

if !executable('rg')
    :finish
endif

set grepprg=rg\ --sort=path\ --vimgrep
set grepformat^=%f:%l:%c:%m

function! Grep(...)
    let results = system(join([&grepprg] + [join(a:000, '')], ' '))
    return results
endfunction

command! -nargs=+ -complete=file_in_path -bar Grep cgetexpr Grep(<f-args>)
command! -nargs=+ -complete=file_in_path -bar LGrep lgetexpr Grep(<f-args>)
command! -nargs=+ -complete=file_in_path -bar Rg cgetexpr Grep(<f-args>)

augroup quickfix
    autocmd!
    autocmd QuickFixCmdPost cgetexpr cwindow
    autocmd QuickFixCmdPost lgetexpr lwindow
augroup END
