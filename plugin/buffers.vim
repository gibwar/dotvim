" Buffer settings

set colorcolumn=+1
set diffopt+=context:3
set diffopt+=filler
set diffopt+=vertical
let &fillchars = "vert:\u2502,fold: "
set hidden
set hlsearch
set ignorecase
set incsearch
set list
let &listchars = "eol:\ue7c3,tab:\u2023\ ,trail:\uf444,extends:\uf6d8,precedes:\uf6d8,nbsp:\u2218"
set matchtime=5
"set nocursorline
set number
set norelativenumber
set scrolloff=2
let &showbreak = "\u21aa "
set showmatch
set signcolumn=number
set splitbelow
set splitright
set smartcase
set updatetime=300

function s:UpdateLeadChars() abort
    let sw = &shiftwidth
    let lead = "│" .. repeat(' ', sw - 1)

    let listchars = &l:listchars
    if !listchars
        let listchars = &g:listchars
    endif

    let &l:listchars = listchars .. ',leadmultispace:' .. lead
endfunction

augroup buffer_cursorline
    autocmd!

    autocmd BufLeave,WinLeave,InsertEnter * set nocursorline
    autocmd BufEnter,WinEnter,InsertLeave * set cursorline
    autocmd BufEnter * call <SID>UpdateLeadChars()
    autocmd OptionSet shiftwidth call<SID>UpdateLeadChars()
augroup END
