" Sets various options for use when editing.

set autoindent
set backspace+=eol
set backspace+=indent
set backspace+=start
set foldmethod=marker
set formatoptions+=c
set formatoptions+=j
set formatoptions+=n
set formatoptions+=q
set formatoptions+=r
set nojoinspaces
set notildeop
set notimeout
set nrformats-=octal
set selection=inclusive
set shiftround
set startofline
set ttimeout
set ttimeoutlen=10
set virtualedit=onemore
set whichwrap=b,s
set wrap
