" For some dumbass reason the maintainer of Polyglot added vim-sensible
" to their plugin startup. Thankfully they allow us to disable it, so
" lets turn that crap off since it jacks with our setup.
let g:polyglot_disabled = ['sensible']
