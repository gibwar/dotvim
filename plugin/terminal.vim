" Sets terminal settings

if has('terminal')
    set termwinkey=<C-Q>
    tnoremap <BS> <C-?>

    if has('win32')
        set termwintype=conpty
    endif

    command TermTab tabnew | terminal ++close ++curwin
endif
