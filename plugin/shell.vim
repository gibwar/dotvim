" Sets shell settings

" external commands outside of the shell need latin1 encoding to properly
" handle parsing the program output.
if has('win32') && executable('pwsh')
    set shell=pwsh
    set shellcmdflag=-NonInteractive\ -ExecutionPolicy\ RemoteSigned\ -Command
    set shellslash

    " pwsh shell quoting and redirection is finally supported in patch 3079 of 8.2
    if !has('patch-8.2.3079')
        set shellquote=\"
        set shellxquote=
        set shellpipe=>%s\ 2>&1
        set shellredir=>%s\ 2>&1
    endif
endif
