" Completion options

set complete^=.
set complete+=w
set complete+=b
set complete+=u
set complete+=t
set complete-=i
set complete-=d

set completeopt+=longest
set completeopt+=menu
set completeopt-=preview
if has('popup')
    set completeopt+=popup
endif

if has('wildmenu')
    set wildcharm=<C-z>

    set wildignore+=**/node_modules/**
    set wildignore+=*.cache,*.class,*.user,*.dll,*.min.*,*.pdb,*.pyc,*.suo
    set wildignore+=*.swp,*.bak
    set wildignore+=*.tar.*

    set wildignorecase
    set wildmenu
    set wildmode=list:longest
endif
