" Contains all mapping commands

let mapleader = "\\"
let maplocalleader = " "

" Make next/previous match open folds and center it in the view.
nnoremap n nzvzz
nnoremap N Nzvzz

" Remap : and ; to make : waaay more convenient. NOTE: this impacts recursive
" maps!
nnoremap ; :
nnoremap : ;
xnoremap ; :
xnoremap : ;

" Remap j and k to go over visual lines rather than actual lines
nnoremap j gj
nnoremap k gk
nnoremap gj j
nnoremap gk k
xnoremap j gj
xnoremap k gk
xnoremap gj j
xnoremap gk k

" Provide a more useful escape key (no stretching!)
inoremap jk <Esc>

" C-Enter in insert mode is quite useful, and a holdover from Visual Studio to
" open a line above.
inoremap <C-CR> <C-o>O

" Pressing C-Tab/C-S-Tab is still habitual, so support my bad behaviors.
nnoremap <C-Tab> gt
nnoremap <C-S-Tab> gT
inoremap <C-Tab> <C-o>gt
inoremap <C-S-Tab> <C-o>gT

" Allow <Tab> to complete the pop-up menu item if it's selected. This is also
" a holdover from Visual Studio muscle memory.
inoremap <silent> <expr> <Tab> pumvisible() ? "\<C-y>" : "\<Tab>"

" Show the buffer list and enable the next command to manipulate it.
"nnoremap <Leader>b :ls<CR>:b

" Map some helper edit/path stuff
" NOTE: !!! the 'nmap' commands use ; instead of : because of the remapping
" done to : and ; !!!
cnoremap <expr> %% getcmdtype() == ':' ? substitute(fnameescape(expand('%:h')), '\', '/', 'g') . '/' : '%%'
nmap <Leader>ee ;e %%
nmap <Leader>es ;sp %%
nmap <Leader>ev ;vsp %%
nmap <Leader>et ;tabe %%

" Map some useful bindings around :find, et al.
nnoremap <Leader>ff :find *
nnoremap <Leader>fs :sfind *
nnoremap <leader>fv :vertical sfind *
nnoremap <Leader>fF :find <C-R>=substitute(expand('%:h'), '\', '/', 'g') . '/*'<CR>
nnoremap <Leader>fS :sfind <C-R>=substitute(expand('%:h'), '\', '/', 'g') . '/*'<CR>
nnoremap <Leader>fV :vertical sfind <C-R>=substitute(expand('%:h'), '\', '/', 'g') . '/*'<CR>

" Map our diff current buffer function
nnoremap <Leader>df :call diff#diff_current()<CR>

" Search for the current word below in the preview window. 2 CRs are needed to
" show that we need to complete the <C-R>= command and then the final command.
nnoremap <Leader>, :psearch <C-R>=expand("<cword>")<CR><CR>

" Window sizing helpers for easy manipulation of panes
nnoremap <M-Right> :vertical resize +3<CR>
nnoremap <M-Left> :vertical resize -3<CR>
nnoremap <M-Up> :resize +3<CR>
nnoremap <M-Down> :resize -3<CR>
nnoremap <M-=> <C-w>=
nnoremap <M--> <C-w>_
nnoremap <M-\> <C-w>\|
nnoremap <M-o> <C-w>_<C-w>\|
nnoremap <Right> <C-w>l
nnoremap <Left> <C-w>h
nnoremap <Up> <C-w>k
nnoremap <Down> <C-w>j

" Toggle spelling using the same keymap as Sublime Text
nnoremap <silent> <F6> :set spell!<CR>
