" Global environment settings that don't really fit in other files.

set history=100
set mouse=nv
set noruler
set noshowcmd
set spelllang=en_us
set ttyfast

" Enable the matchit plugin (it's included in the distribution now)
packadd matchit
