#! pwsh

$definition = @"
[DllImport("user32.dll")] private static extern int GetAwarenessFromDpiAwarenessContext(IntPtr context);
[DllImport("user32.dll")] private static extern IntPtr GetThreadDpiAwarenessContext();
[DllImport("user32.dll")] private static extern IntPtr SetThreadDpiAwarenessContext(IntPtr context);
[DllImport("user32.dll")] private static extern bool SystemParametersInfoA(uint uiAction, uint uiParam, ref Rect pvParam, uint fWinIni);
[StructLayout(LayoutKind.Sequential)] private struct Rect { public int left; public int top; public int right; public int bottom; }
public static void GetWorkArea()
{
    var context = GetThreadDpiAwarenessContext();
    var awareness = GetAwarenessFromDpiAwarenessContext(context);
    if (awareness <= 0) {
        context = SetThreadDpiAwarenessContext(new IntPtr(-4));
        if (context == IntPtr.Zero)
            throw new Exception("Unable to set context");
    }

    var rect = new Rect();
    SystemParametersInfoA(0x0030, 0, ref rect, 0);
    SetThreadDpiAwarenessContext(context);
    Console.WriteLine(
        @"{{""top"":{0},""left"":{1},""bottom"":{2},""right"":{3}}}",
        rect.top, rect.left, rect.bottom, rect.right);
}
"@

Add-Type -MemberDefinition $definition -Name "Dpi" -Namespace "Scripts"

[Scripts.Dpi]::GetWorkArea()
