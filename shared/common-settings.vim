" shared settings for filetypes with similar types (react/js/ts/etc).

call undoftplugin#setl('expandtab')
call undoftplugin#setl('shiftwidth', 4)
call undoftplugin#setl('signcolumn', 'yes')
call undoftplugin#setl('softtabstop', 4)
call undoftplugin#setl('tabstop', 8)
call undoftplugin#setl('textwidth', 120)

call undoftplugin#setl('foldmethod', 'syntax')
call undoftplugin#setl('foldlevel', 1)

function! s:save_fdm()
    if exists('w:last_fdm')
        return
    endif

    let w:last_fdm = &l:foldmethod
    setlocal foldmethod=manual
endfunction

function! s:restore_fdm()
    if !exists('w:last_fdm')
        return
    endif

    let &l:foldmethod = w:last_fdm
    unlet w:last_fdm
endfunction

augroup buffer_cmds
    autocmd! * <buffer>

    autocmd BufWritePre  <buffer> %s/\s\+$//e
    autocmd CompleteDone <buffer> pclose
    autocmd InsertEnter  <buffer> call s:save_fdm()
    autocmd InsertLeave,WinLeave <buffer> call s:restore_fdm()
augroup END
call undoftplugin#add_autocmd("buffer_cmds")
