let s:has_ctags = 0
let s:ctags_prog = ''

function! tags#run_ctags() abort
    if !tags#has_ctags()
        echoerr 'tags#run_ctags(): ctags executable not found'
        return
    endif

    let l:git_dir = fnamemodify(FugitiveExtractGitDir('.'), ':p:gs?/?\\?')
    let l:project_dir = fnamemodify(l:git_dir, ':h:h')

    call job_start(['git', 'ls-files'], #{
                \ cwd: l:project_dir,
                \ err_io: 'null',
                \ exit_cb: function('s:git_files_done', [l:git_dir, l:project_dir]),
                \ in_io: 'null',
                \ out_io: 'buffer',
                \ out_msg: 0,
                \ })
endfunction

function! tags#has_ctags() abort
    call s:FindCtags()
    if s:has_ctags == 1
        return 1
    endif
    return 0
endfunction

function! <SID>FindCtags() abort
    let l:ctags = fnamemodify('~/programs/ctags/ctags.exe', ':p:gs?/?\\?')
    if filereadable(l:ctags)
        let s:has_ctags = 1
        let s:ctags_prog = l:ctags
        return
    endif

    let s:has_ctags = -1
    let s:ctags_prog = ''
endfunction

function! s:ctags_done(in_buf_nr, job, status) abort
    if a:status != 'dead'
        echoerr 'tags#run_ctags(): ctags exit_cb called when status was not dead: ' .. a:status
        return
    endif

    let l:exit_code = a:job->job_info()['exitval']
    if l:exit_code != 0
        echoerr 'tags#run_ctags(): ctags returned non-zero exit code: ' .. l:exit_code
        return
    endif

    if a:in_buf_nr > -1
        silent! execute ':bwipeout' a:in_buf_nr
    endif
    echo 'tags#run_ctags(): tags updated'
endfunction

function! s:git_files_done(git_dir, project_dir, job, status) abort
    if a:status != 'dead'
        echoerr 'tags#run_ctags(): git exit_cb called when status was not dead: ' .. a:status
        return
    endif

    let l:exit_code = a:job->job_info()['exitval']
    if l:exit_code != 0
        echoerr 'tags#run_ctags(): git ls-files returned non-zero exit code: ' .. l:exit_code
        return
    endif

    let l:in_buf_nr = a:job->job_getchannel()->ch_getbufnr('out')
    let l:options = '--options=' .. a:git_dir .. 'ctags'
    let l:tag_file = a:git_dir .. 'tags'

    let l:ctags = [s:ctags_prog, l:options, '-L', '-', '--tag-relative', '-f', l:tag_file]
    call job_start(l:ctags, #{
                \ cwd: a:project_dir,
                \ in_io: 'buffer',
                \ in_buf: l:in_buf_nr,
                \ out_io: 'null',
                \ err_io: 'null',
                \ exit_cb: function('s:ctags_done', [l:in_buf_nr]),
                \ })
endfunction

