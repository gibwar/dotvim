function! diff#diff_current() abort
    " store the current filetype for syntax highlighting
    let filetype = &filetype

    " create a new tab from the current buffer
    tab split

    " open a new vertical split
    vnew

    " read the original file in to the buffer
    0read #

    " delete the trailing line
    $d

    " set buffer options to disappear on close
    setlocal buftype=nofile
    setlocal bufhidden=wipe
    setlocal nobuflisted
    setlocal noswapfile
    setlocal readonly
    execute "setlocal filetype=" . filetype

    " mark the windows as participating in the diff
    windo diffthis

    " move focus back to the unsaved buffer
    wincmd p

    " undo any folding by diffthis
    setlocal foldlevel=99
endfunction
