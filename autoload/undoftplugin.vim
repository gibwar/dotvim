function! s:ensure_undo_ftplugin() abort
    if get(b:, 'undo_ftplugin', '') == ''
        let b:undo_ftplugin = ''
    endif
endfunction

function! undoftplugin#add(undo_command) abort
    call s:ensure_undo_ftplugin()

    if b:undo_ftplugin == ''
        let b:undo_ftplugin = a:undo_command
        return
    endif

    let b:undo_ftplugin .= '|' . a:undo_command
endfunction

function! undoftplugin#add_autocmd(autocmd) abort
    let undo_autocmd = 'autocmd! ' . a:autocmd
    call undoftplugin#add(undo_autocmd)
endfunction

function! undoftplugin#setl(option, value = v:none) abort
    let command = ''
    if a:value isnot v:none
        let command = 'setlocal ' . a:option . '=' . a:value
    else
        let command = 'setlocal ' . a:option
    endif

    let undo_command = 'setlocal ' . a:option . '<'

    execute command
    call undoftplugin#add(undo_command)
endfunction
