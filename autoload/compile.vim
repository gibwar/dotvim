function! compile#run(...) abort
    " save files before running
    silent update

    let original_compiler = get(b:, 'current_compiler', '')
    let combined_error_format = []
    let quick_fix_list = []
    for cmp in a:000
        execute 'compiler ' .. cmp
        call add(combined_error_format, &errorformat)

        echon 'Running ' .. cmp
        execute 'silent! make %'
        call extend(quick_fix_list, getqflist())
    endfor

    echon ''
    let &errorformat = join(combined_error_format, ',')
    call setqflist(quick_fix_list)
    cwindow

    if !empty(quick_fix_list)
        cfirst
    endif

    if original_compiler isnot ''
        let b:current_compiler = original_compiler
    endif
endfunction
