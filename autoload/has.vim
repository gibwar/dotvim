" vim: ts=4:sw=4:sts=4:et:ff=dos:ft=vim

function! has#colorscheme(name) abort
    let path = 'colors/' . a:name . '.vim'
    return !empty(globpath(&runtimepath, path))
endfunction

