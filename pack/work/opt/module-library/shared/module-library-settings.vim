" shared settings for filetypes with similar settings.
" designed to support the module library.

call undoftplugin#setl('expandtab')
call undoftplugin#setl('shiftwidth', 2)
call undoftplugin#setl('signcolumn', 'yes')
call undoftplugin#setl('softtabstop', 2)
call undoftplugin#setl('tabstop', 8)
call undoftplugin#setl('textwidth', 100)

call undoftplugin#setl('foldmethod', 'syntax')
call undoftplugin#setl('foldlevel', 2)
