let s:coc_extensions = [
    \'coc-css',
    \'coc-html',
    \'coc-json',
    \'coc-tsserver',
    \'coc-styled-components',
    \]

if exists('g:coc_only_extensions')
    let g:coc_global_extensions = copy(g:coc_only_extensions)
elseif exists('g:coc_global_extensions')
    let g:coc_global_extensions += s:coc_extensions
    call uniq(sort(g:coc_global_extensions))
else
    let g:coc_global_extensions = copy(s:coc_extensions)
endif

unlet s:coc_extensions
packadd coc

" helps when pressing backspace when the popup menu is open
function! CheckBackspace() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1] =~# '\s'
endfunction

" enable tab completion with the new popup menu
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" enable <CR> to trigger coc compation confirm (both for popup
" menu and snippet additions)
inoremap <expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

" trigger popup with control space or control-shift-2
inoremap <silent><expr> <C-Space> coc#refresh()
inoremap <silent><expr> <C-@> coc#refresh()

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nnoremap <silent> <leader>D :CocDiagnostics<CR>

" GoTo code navigation
nnoremap <silent> gD gd
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gI <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Symbol renaming
nmap <silent><leader>rn <Plug>(coc-rename)

" Formatting selected code
xmap <silent><leader>f <Plug>(coc-format-selected)
nmap <silent><leader>f <Plug>(coc-format-selected)

" Applying code actions to the selected code block
" Example: `<leader>aap` for current paragraph
xmap <silent><leader>a <Plug>(coc-codeaction-selected)
nmap <silent><leader>a <Plug>(coc-codeaction-selected)

" Remap keys for applying code actions at the cursor position
nmap <silent><leader>ac <Plug>(coc-codeaction-cursor)
" Remap keys for apply code actions affect whole buffer
nmap <silent><leader>as <Plug>(coc-codeaction-source)
" Apply the most preferred quickfix action to fix diagnostic on the current line
nmap <silent><leader>qf <Plug>(coc-fix-current)

" Remap keys for applying refactor code actions
nmap <silent><leader>re <Plug>(coc-codeaction-refactor)
xmap <silent><leader>r <Plug>(coc-codeaction-refactor-selected)
nmap <silent><leader>r <Plug>(coc-codeaction-refactor-selected)

" Run the Code Lens action on the current line
nmap <silent><leader>cl <Plug>(coc-codelens-action)

" Remap <C-f> and <C-b> to scroll float windows/popups
if has('patch-8.2.0750')
    nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll()
       \ ? coc#float#scroll(1) : "\<C-f>"
    nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll()
       \ ? coc#float#scroll(0) : "\<C-b>"
    inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll()
       \ ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<C-f>"
    inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll()
       \ ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<C-b>"
    vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll()
       \ ? coc#float#scroll(1) : "\<C-f>"
    vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll()
       \ ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges
" Requires 'textDocument/selectionRange' support of language server
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer
command! -nargs=? Fold :call CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer
command! -nargs=0 OR :call CocActionAsync('runCommand', 'editor.action.organizeImport')

" Mappings for CoCList
" Show all diagnostics
nnoremap <silent><nowait> <leader>cca :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent><nowait> <leader>cce :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent><nowait> <leader>ccc :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent><nowait> <leader>cco :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent><nowait> <leader>ccs :<C-u>CocList -I symbols<cr>
" Do default action for next item
nnoremap <silent><nowait> <leader>ccj :<C-u>CocNext<CR>
" Do default action for previous item
nnoremap <silent><nowait> <leader>cck :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent><nowait> <leader>ccp :<C-u>CocListResume<CR>

" coc autocommands
augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s)
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Use K to show documentation in preview window
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
    if coc#rpc#ready() == 1 && CocAction('hasProvider', 'hover')
        call CocActionAsync('doHover')
    else
        call feedkeys('K', 'in')
    endif
endfunction

" start coc after a couple seconds of vim start and session loading
call timer_start(2000, {-> execute('CocStart') })
